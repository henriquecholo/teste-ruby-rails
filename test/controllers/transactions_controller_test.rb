require 'test_helper'

class TransactionsControllerTest < ActionController::TestCase
  setup do
    @account = accounts(:one)
    @transaction = transactions(:one)
  end

  test "should get index" do
    get :index, account_id: @account
    assert_response :success
    assert_not_nil assigns(:transactions)
  end

  test "should get new" do
    get :new, id: @transaction, account_id: @account
    assert_response :success
  end

  test "should create transaction" do
    assert_difference('Transaction.count') do
      post :create, transaction: { account_id: @account, transaction_rate: @transaction.transaction_rate, transaction_total: @transaction.transaction_total, transaction_value: @transaction.transaction_value, transaction_type: @transaction.transaction_type, transferred_account: @transaction.transferred_account }, account_id: @account
    end
  end

  test "should show transaction" do
    get :show, id: @transaction, account_id: @account
    assert_response :success
  end

  test "should update transaction" do
    patch :update, id: @transaction, transaction: { account_id: @account, transaction_rate: @transaction.transaction_rate, transaction_total: @transaction.transaction_total, transaction_value: @transaction.transaction_value, transaction_type: @transaction.transaction_type, transferred_account: @transaction.transferred_account }
    assert_redirected_to transaction_path(assigns(:transaction))
  end

  test "should destroy transaction" do
    assert_difference('Transaction.count', -1) do
      delete :destroy, id: @transaction
    end

    assert_redirected_to transactions_path
  end
end
