require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  setup do
    @account = accounts(:one)
    @account_two = accounts(:two)
    @transaction = transactions(:one)
    @transaction.account_id = @account.id
    @transaction.transferred_account = @account_two.id
    @transaction_two = transactions(:two)
    @transaction_two.account_id = @account_two.id
    @transaction_two.transferred_account = @account.id
  end

  test "should calculate rate" do
    assert @transaction.calculate_rate == 5 || @transaction.calculate_rate == 7
  end

  test "should calculate rates and total properly" do
    @transaction.calculate_rates_and_total
    if @transaction.is_transfer
      assert_not_equal(@transaction.transaction_value, @transaction.transaction_total)
      assert @transaction.transaction_rate == 5 || @transaction.transaction_rate == 7
    else
      assert_equal(@transaction.transaction_value, @transaction.transaction_total)
      assert_equal(@transaction.transaction_rate, 0)
    end
  end

  test "should calculate big values rate properly" do
    assert @transaction.calculate_big_values_rate  == 0 || @transaction.calculate_big_values_rate == 10
  end

  test "should set new bank balance to account owner properly" do
    @account = Account.find(@transaction.account_id)
    @transaction.set_new_bank_balance_to_account_owner
    assert_not_equal(@transaction.account.bank_balance, @account.bank_balance)
  end

  test "should set_new bank balance to transferred owner properly" do
    @transferred_account = Account.find(@transaction.transferred_account)
    @transaction.set_new_bank_balance_to_transferred_owner
    assert_not_equal(@transaction.transferred_account_object.bank_balance, @transferred_account.bank_balance)
  end
end
