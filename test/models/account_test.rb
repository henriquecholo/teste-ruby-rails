require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  setup do
    @account = accounts(:one)
    @account_two = accounts(:two)
  end
  test "should get names that are not closed" do
    assert_equal(Account.get_names_not_closed(@account), [["Henrique Mello", @account_two.id]])
    assert_equal(Account.get_names_not_closed(@account_two), [["Henrique", @account.id]])
  end
end
