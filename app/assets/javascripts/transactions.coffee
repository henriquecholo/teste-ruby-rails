# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  transaction_type_select = $("select#transaction_transaction_type");
  transfer_select = $("#transfer_select__form_group")
  if(transaction_type_select.val() != '3')
    transfer_select.fadeOut();
  transaction_type_select.on "change", (event) ->
    event.preventDefault();
    option = $(this).val();
    if option == '3'
      if $("#transfer_select__form_group option").length > 1
        transfer_select.fadeIn();
      else
        alert('Não existem contas para transferência.')
        $(this).val('1');
    else
      transfer_select.fadeOut();

  $.rails.allowAction = (link) ->
    return true unless $("select#transaction_transaction_type").val() != "1"
    return true unless link.attr('data-confirm')
    $.rails.showConfirmDialog(link)
    false

  $.rails.confirmed = (link) ->
    link.removeAttr('data-confirm')
    link.trigger('click.rails')

  $.rails.showConfirmDialog = (link) ->
    $('#confirmation').modal()
    $('#confirmation .confirm').on 'click', ->
      $.rails.confirmed(link)

$(document).ready(ready)
$(document).on('page:load', ready)