json.array!(@accounts) do |account|
  json.extract! account, :id, :name, :password, :bank_balance, :closed
  json.url account_url(account, format: :json)
end
