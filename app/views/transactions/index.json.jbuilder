json.array!(@transactions) do |transaction|
  json.extract! transaction, :id, :transaction_type, :transaction_value, :transaction_rate, :transaction_total, :account_id
  json.url transaction_url(transaction, format: :json)
end
