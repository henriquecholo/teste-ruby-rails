class Transaction < ActiveRecord::Base
  belongs_to :account
  accepts_nested_attributes_for :account
  attr_accessor :password_confirmation, :transferred_account_object
  before_validation :calculate_rates_and_total
  before_create :set_bank_balance
  validate :password
  validate :does_not_have_transfer
  validate :does_not_have_funds
  validates_associated :account
  validates_numericality_of :transaction_value,
    :on => :create,
    :greater_than => 0,
    :message => 'deve ser maior que 0.'
  scope :created_between, lambda {|start_date, end_date|
    if start_date && end_date
      where(:created_at => Date.new(start_date[:year].to_i, start_date[:month].to_i, start_date[:day].to_i)..
            Date.new(end_date[:year].to_i, end_date[:month].to_i, end_date[:day].to_i))
    end
  }

  def password
    if !is_deposit
      errors[:base] << 'Senha não corresponde a do usuário da conta.' if self.password_confirmation != account.password
    end
  end

  def does_not_have_transfer
    if is_transfer
      errors[:base] << 'Você precisa escolher uma conta.' if !transferred_account
    end
  end

  def does_not_have_funds
    if !is_deposit
      errors[:base] <<  'Você não possui fundos suficientes.' if account.bank_balance < transaction_total
    end
  end

  def calculate_rates_and_total
    if !is_transfer
      self.transaction_rate = 0
      self.transaction_total = transaction_value
    else
      self.transaction_rate = calculate_rate
      self.transaction_total = transaction_value + transaction_rate + calculate_big_values_rate
    end
  end

  def calculate_rate
    _date = Time.now
    _week = _date.strftime('%w')
    _hour = _date.strftime('%T')
    _week != 0 and _week != 6 and _hour >= '09:00:00' and _hour < '18:00:00' ? 5 : 7
  end

  def calculate_big_values_rate
    transaction_value > 1000 ? 10 : 0
  end

  def set_bank_balance
    set_new_bank_balance_to_account_owner
    if is_transfer
      set_new_bank_balance_to_transferred_owner
    end
  end

  def set_new_bank_balance_to_account_owner
    self.account.bank_balance = is_deposit ? account.bank_balance + transaction_total : account.bank_balance - transaction_total
  end

  def set_new_bank_balance_to_transferred_owner
    self.transferred_account_object = Account.find(transferred_account)
    self.transferred_account_object.bank_balance = self.transferred_account_object.bank_balance + transaction_value
  end

  def is_transfer
    transaction_type == 3
  end

  def is_deposit
    transaction_type == 1
  end

end