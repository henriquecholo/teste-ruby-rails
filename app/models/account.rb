class Account < ActiveRecord::Base
  has_many :transactions
  validates :bank_balance,
    :presence => true
  scope :get_names_not_closed, lambda {|account_id|
    where.not(id: account_id, closed: 1).collect {|p| [ p.name, p.id.to_i() ] }
  }
end
