module TransactionsHelper
  def translate_transaction_type_helper(transaction_type)
    if transaction_type == 1
      'Depósito'
    elsif transaction_type == 2
      'Saque'
    elsif transaction_type == 3
      'Transferência'
    else
      ''
    end
  end
end
