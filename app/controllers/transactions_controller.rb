class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]

  # GET /transactions
  # GET /transactions.json
  def index
    @transactions = Account.find(params[:account_id]).transactions.created_between(params[:created_at_gte], params[:created_at_lt])
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.account = Account.find(params[:account_id])
    respond_to do |format|
      if @transaction.save and @transaction.account.save and
          (!@transaction.transferred_account_object || @transaction.transferred_account_object.save)
        format.html { redirect_to @transaction.account, notice: 'Transação realizada com sucesso.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transação alterada com sucesso' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transação excluída com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.delete(:created_at_gte)
      params.delete(:created_at_lt)
      params.delete(:utf8)
      params.delete(:commit)
      params.delete(:authenticity_token)
      params.require(:transaction).permit(:transaction_type, :transaction_value, :transaction_rate, :transaction_total, :account_id, :transferred_account, :created_at, :password_confirmation)
    end
end