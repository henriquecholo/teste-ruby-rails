class AddColumnToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :transferred_account, :integer
  end
end
