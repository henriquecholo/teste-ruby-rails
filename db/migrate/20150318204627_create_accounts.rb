class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :name
      t.string :password
      t.float :bank_balance
      t.boolean :closed

      t.timestamps null: false
    end
  end
end
