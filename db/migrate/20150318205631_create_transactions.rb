class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :type
      t.float :transaction_value
      t.integer :transaction_rate
      t.float :transaction_total
      t.references :account, index: true

      t.timestamps null: false
    end
    add_foreign_key :transactions, :accounts
  end
end
